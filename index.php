<?php

/*
 * ---------------------------------------------------------
 *  APPLICATION ENVIRONMENT
 * ---------------------------------------------------------
 * 
 */

define('ENVIRONMENT', 'development');


/*
 * --------------------------------------------------------
 *  ERROR REPORTING
 * --------------------------------------------------------
 * 
 */

if (defined('ENVIRONMENT')) {

    switch (ENVIRONMENT) {
        case 'development':
                error_reporting(E_ALL);
            break;
        case 'testing':
        case 'production':
                error_reporting(0);
            break;
        default :
            exit('Specify Application Environment');
    }
}

define('ROOT', dirname(__FILE__));
define('DIR_SEP', DIRECTORY_SEPARATOR);


/*
 * ------------------------------------------------------
 *  APPLICATION FOLDER
 * ------------------------------------------------------
 */

$application_folder = 'application';

/*
 * ------------------------------------------------------
 *  SYSTEM FOLDER
 * ------------------------------------------------------
 */

$system_folder = 'system';

define('SELF', pathinfo(__FILE__, PATHINFO_BASENAME));

//define('BASEPATH', str_replace("\\", "/", $system_folder));

if (realpath($system_folder) !== FALSE) {
    $system_folder = realpath($system_folder) . '/';
}

$system_folder = rtrim($system_folder, '/').'/';

if( !is_dir($system_folder) ){
    exit('Your System Folder Does Not Appear To Be Set Correctly.');
}else{
    define( 'BASEPATH', $system_folder );
}

//var_dump( BASEPATH );

if(is_dir( $application_folder ) ){
    defined( 'APPPATH', $application_folder.'/' );
}else if( is_dir( BASEPATH.$application_folder ) ){
    defined( 'APPPATH', BASEPATH.$application_folder.'/' );
}else{
    exit('Your Application Folder Does Not Appear To Be Set Correctly.');
}

define('DS', DIRECTORY_SEPARATOR);
 
$url = $_GET['url'];
 
require BASEPATH.'bootstrap.php';

