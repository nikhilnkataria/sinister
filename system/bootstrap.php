<?php

/*
 * --------------------------------------------------------
 *  Loading Config Files
 * --------------------------------------------------------
 */

if( defined( 'ENVIRONMENT' ) && file_exists( APPPATH.'config/'.ENVIRONMENT.'/constants.php' ) ){
    require APPPATH.'config/'.ENVIRONMENT.'/constants.php';
}else{
    require APPPATH.'config/constants.php';
}

/*
 * --------------------------------------------------------
 *  Exception Handling Will Come Here
 * --------------------------------------------------------
 */